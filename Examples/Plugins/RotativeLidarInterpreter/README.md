# Rotative LiDAR Interpreter Example

This plugin showcases an implementation of a Rotative Lidar Interpreter.

All necessary implementations are illustrated in this example, which
should be kept updated as development progresses.

### File Architecture and how to run it

It follow the same architecture than [Time Based Lidar Interpreter Architecture](../TimeBasedLidarInterpreter/README.md#file-architecture).
The primary differences are the type of data being received and the conditions used to split frames.

