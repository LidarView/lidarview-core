set(classes
  vtkPCDWriter)

vtk_module_add_module(LidarView::IOGeneral
  CLASSES ${classes})
